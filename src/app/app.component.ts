import { Component } from '@angular/core';
import * as Sentry from "@sentry/angular";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'devops';

  constructor() {
    try {
      throw new Error('Something bad happened');
    }
    catch(e) {
      Sentry.captureException(e);
    }
  }

}
