# Resumen

[Da el resumen del issue]

## Comportamiento deseado

[Describe el comportamiento deseado]

## Comportamiento actual

[Describe el comportamiento actual]

## Pasos para reproducir

[Indica los pasos para reproducir el error]